# Steganography Client #

A desktop client for steganography

### _Table of Contents_

- [How to Run on Mac](#How to Run on Mac)
- [How to Run on Ubuntu](#How to Run on Ubuntu)

### How to Run on Mac

1. Install `cmake` on your mac using `brew install cmake` 
2. Install `boost` on your mac using `brew install boost`
3. Install `QT5` on your mac using `brew install qt@5`
4. Build using cmake


### How to Run on Ubuntu (Steps tested with Ubuntu 19.10)

1. Install cmake, boost, QT5
2. Build using cmake
