#ifndef STEGO_CLIENT_MAIN_WINDOW_HPP
#define STEGO_CLIENT_MAIN_WINDOW_HPP

#include <QWidget>
#include <QApplication>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QUrlQuery>
#include <QHttpMultiPart>

#include "ui_main_window.h"

#include <boost/chrono.hpp>
#include <boost/thread/thread.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include <fstream>

class MainWindow : public QMainWindow {
    Q_OBJECT

    public :
        MainWindow();
        ~MainWindow();

    private :
        Ui::MainWindow ui;

        QNetworkAccessManager *network_manager_;

        bool run_srv_health_chk_;
        boost::thread* srv_health_chk_thread_;
        boost::mutex connection_mtx_;

        std::string server_url_;
        std::string encrypt_ep_;
        std::string decrypt_ep_;
        std::string health_ep_;

        std::string health_url_;
        std::string encrypt_url_;
        std::string decrypt_url_;

        std::string output_img_f_;
        std::string input_img_f_;

        void checkServerStatus();

        void connectUI();
        void initParams();
        void setupNetworkManager();
        void updateConnectionStatus(bool);
        void clearEncryptLog();
        void updateEncryptLog(std::string);
        void handleEncryptResponse(QNetworkReply*);
        void clearDecryptLog();
        void updateDecryptLog(std::string);
        void handleDecryptResponse(QNetworkReply*);

        enum RESPONSE_TYPE {
            ENCRYPT,
            DECRYPT,
            HEALTH,
            INVALID
        };

    private slots :
        void updateServerSettings();
        void checkServerHealth(QNetworkRequest);
        void networkManagerFinished(QNetworkReply*);
        void encryptBtnClicked();
        void decryptBtnClicked();

    Q_SIGNALS :
        void signal_checkServerHealth(QNetworkRequest);
};

#endif