#include <main_window.hpp>

void MainWindow::decryptBtnClicked() {
	ui.btn_decrypt->setEnabled(false);

	input_img_f_ = ui.decrypt_img_path->text().toStdString();
    std::string pw = ui.decrypt_pw_text->text().toStdString();

    if (pw.empty() || input_img_f_.empty()) {
        updateDecryptLog("Image path or the password is empty");
        ui.btn_decrypt->setEnabled(true);
        return;
    } else {
        // std::cout << pw << std::endl;
    }

    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart textPart;
    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"Password\""));
    textPart.setBody(pw.c_str());

    QHttpPart imagePart;
    imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
    imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"image\""));
    QFile *file = new QFile(input_img_f_.c_str());
    file->open(QIODevice::ReadOnly);
    imagePart.setBodyDevice(file);
    file->setParent(multiPart); 

    multiPart->append(textPart);
    multiPart->append(imagePart);

    QNetworkRequest request;

    connection_mtx_.lock();
    request.setUrl(QUrl(decrypt_url_.c_str()));
    connection_mtx_.unlock();

    network_manager_->post(request, multiPart);
    updateDecryptLog("Request sent to server");
    updateDecryptLog("Waiting...");
}

void MainWindow::handleDecryptResponse(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        updateDecryptLog("Server responded");

        qint64 maxSize = 1024;
        char* data = new char[maxSize];
        qint64 l = reply->read(data, maxSize);
        int count = 0;
        std::string t = "Read ";
        std::string msg = "";

        updateDecryptLog("Reading data :");

        while (l > 0) {
            for (int i=0;i<l;i++) {
                msg.push_back(data[i]);
            }
            updateDecryptLog(t + boost::lexical_cast<std::string>(l) + " bytes...");
            l = reply->read(data, maxSize);
        }
        
        ui.decrypt_msg_text->setText(QString(msg.c_str()));
        updateDecryptLog("Done!");
        
    } else {
        // std::cout << "Error" << std::endl;
        updateDecryptLog("Error received from the server");
    }
    ui.btn_decrypt->setEnabled(true);

}

void MainWindow::updateDecryptLog(std::string msg) {
	ui.decrypt_log->append(QString(msg.c_str()));
}

void MainWindow::clearDecryptLog() {
	ui.decrypt_log->setText("");
}