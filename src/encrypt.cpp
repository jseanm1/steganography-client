#include <main_window.hpp>

void MainWindow::encryptBtnClicked() {
	ui.btn_encrypt->setEnabled(false);

	output_img_f_ = ui.encrypted_img_path->text().toStdString();
    std::string pw = ui.encrypt_pw_text->text().toStdString();
    std::string msg = ui.encrypt_msg_text->toPlainText().toStdString();

    if (pw.empty() || msg.empty() || output_img_f_.empty()) {
        updateEncryptLog("Image path, message or the password is empty");
        ui.btn_encrypt->setEnabled(true);
        return;
    } else {
        // std::cout << msg << std::endl;
        // std::cout << pw << std::endl;
    }   

    QNetworkRequest request;

    connection_mtx_.lock();
    request.setUrl(QUrl(encrypt_url_.c_str()));
    connection_mtx_.unlock();

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

    QUrlQuery postData;
	postData.addQueryItem("password", pw.c_str());
	postData.addQueryItem("message", msg.c_str());

    network_manager_->post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
    updateEncryptLog("Request sent to server");
    updateEncryptLog("Waiting...");
}

void MainWindow::handleEncryptResponse(QNetworkReply* reply) {
	updateEncryptLog("Response received from the server");

    if (reply->error() == QNetworkReply::NoError) {

    	std::ofstream outputFile(output_img_f_.c_str());

    	if (!outputFile.is_open()) {
    		updateEncryptLog("Error writing to file : IO Error");
    		updateEncryptLog("Check if the location exists and if you have permission");
    		ui.btn_encrypt->setEnabled(true);
    		return;
    	}

    	qint64 maxSize = 1024;
    	char* data = new char[maxSize];
    	qint64 l = reply->read(data, maxSize);
    	int count = 0;
    	std::string t = "Wrote ";

    	updateEncryptLog("Writing to file :");
    	while (l > 0) {
    		outputFile.write(data, l);
    		updateEncryptLog(t + boost::lexical_cast<std::string>(l) + " bytes...");
    		l = reply->read(data, maxSize);
    	}

    	outputFile.close();
    	updateEncryptLog("Successfuly wrote file!");
    	updateEncryptLog(output_img_f_);
    } else {
    	// std::cout << "Error" << std::endl;
        updateEncryptLog("Error received from the server");
    }
	ui.btn_encrypt->setEnabled(true);
}

void MainWindow::updateEncryptLog(std::string msg) {
	ui.encrypt_log->append(QString(msg.c_str()));
}

void MainWindow::clearEncryptLog() {
	ui.encrypt_log->setText("");
}
