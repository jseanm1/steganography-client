#include <main_window.hpp>

void MainWindow::checkServerStatus() {
    bool status = false;

    while (run_srv_health_chk_) {
        QNetworkRequest request;

        connection_mtx_.lock();
        request.setUrl(QUrl(health_url_.c_str()));
        connection_mtx_.unlock();

        Q_EMIT signal_checkServerHealth(request);

        boost::this_thread::sleep_for(boost::chrono::milliseconds(5000));
    }

    std::cout << "Stopping server health check" << std::endl;
}