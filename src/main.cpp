#include <iostream>

#include <QWidget>
#include <QApplication>

#include <main_window.hpp>

int main (int argc, char** argv) {
    std::cout << "Steganography Client" << std::endl;

    QApplication app(argc, argv);
    MainWindow w;

    w.show();
    app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));
    
    int result = app.exec();
    std::cout << "App returned " << result << " on close" << std::endl;
}
