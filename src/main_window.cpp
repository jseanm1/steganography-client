#include <main_window.hpp>

MainWindow::MainWindow() :
    run_srv_health_chk_(true)
{
    ui.setupUi(this);
    connectUI();
    initParams();

    network_manager_ = new QNetworkAccessManager();
    setupNetworkManager();

    srv_health_chk_thread_ = new::boost::thread(&MainWindow::checkServerStatus, this);

    std::cout << "Main Window up!" << std::endl;
}

MainWindow::~MainWindow() {
    std::cout << "Main Window destructor called" << std::endl;

    run_srv_health_chk_ = false;

    srv_health_chk_thread_->join();

    delete network_manager_;
}

void MainWindow::setupNetworkManager() {
    connect(network_manager_, SIGNAL(finished(QNetworkReply*)), this, SLOT(networkManagerFinished(QNetworkReply*)));
    connect(this, SIGNAL(signal_checkServerHealth(QNetworkRequest)), this, SLOT(checkServerHealth(QNetworkRequest)));
}

void MainWindow::connectUI() {
    connect(ui.btn_update_srv_settings, SIGNAL(clicked()), this, SLOT(updateServerSettings()));
    connect(ui.btn_encrypt, SIGNAL(clicked()), this, SLOT(encryptBtnClicked()));
    connect(ui.btn_decrypt, SIGNAL(clicked()), this, SLOT(decryptBtnClicked()));
}

void MainWindow::initParams() {
    updateServerSettings();
}

void MainWindow::updateServerSettings() {
    connection_mtx_.lock();
    server_url_ = ui.server_address_text->text().toStdString();
    encrypt_ep_ = ui.encrypt_ep_text->text().toStdString();
    decrypt_ep_ = ui.decrypt_ep_text->text().toStdString();
    health_ep_ = ui.health_ep_text->text().toStdString();

    health_url_ = server_url_ + health_ep_;
    encrypt_url_ = server_url_ + encrypt_ep_;
    decrypt_url_ = server_url_ + decrypt_ep_;
    connection_mtx_.unlock();
}

void MainWindow::updateConnectionStatus(bool ok) {
    QPalette pe;
    std::string msg;

    if (ok) {
        pe.setColor(QPalette::WindowText, Qt::green);
        msg = "Connected!";
    } else {
        pe.setColor(QPalette::WindowText, Qt::red);
        msg = "Not connected!";
    }

    ui.connection_status_1->setPalette(pe);
    ui.connection_status_2->setPalette(pe);
    ui.connection_status_3->setPalette(pe);

    ui.connection_status_1->setText(msg.c_str());
    ui.connection_status_2->setText(msg.c_str());
    ui.connection_status_3->setText(msg.c_str());
}

void MainWindow::checkServerHealth(QNetworkRequest request) {
    network_manager_->get(request);
}

void MainWindow::networkManagerFinished(QNetworkReply* reply) {
    std::string request_url = reply->request().url().url().toStdString();

    int response_type;

    connection_mtx_.lock();
    if (request_url == encrypt_url_) {
        response_type = RESPONSE_TYPE::ENCRYPT;
    } else if (request_url == decrypt_url_) {
        response_type = RESPONSE_TYPE::DECRYPT;
    } else if (request_url == health_url_) {
        response_type = RESPONSE_TYPE::HEALTH;
    } else {
        response_type = RESPONSE_TYPE::INVALID;
    }
    connection_mtx_.unlock();

    switch (response_type) {
        case RESPONSE_TYPE::ENCRYPT :
            handleEncryptResponse(reply);
        break;

        case RESPONSE_TYPE::DECRYPT :
            handleDecryptResponse(reply);
        break;

        case RESPONSE_TYPE::HEALTH :
            if (reply->error() == QNetworkReply::NoError) {
                updateConnectionStatus(true);
            } else {
                updateConnectionStatus(false);
            }
        break;

        default :
            std::cout << "EXPIRED / INVALID URL from RESPONSE" << std::endl;
    }
}